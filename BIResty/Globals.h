//
//  Globals.h
//  BIRestyLibrary
//
//  Created by Jan Galler on 04/06/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#ifndef BIRestyLibrary_Globals_h
#define BIRestyLibrary_Globals_h

// HTTPMethod Type
typedef enum
{
    HTTPMethodGET,
    HTTPMethodPOST,
    HTTPMethodPUT,
    HTTPMethodDELETE
}HTTPMethod;


#endif
