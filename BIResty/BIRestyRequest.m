//
//  BIRestyRequest.m
//  BIRestyLibrary
//
//  Created by Jan Galler on 04/06/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import "BIRestyRequest.h"

@interface BIRestyRequest ()

@property (strong, nonatomic) BIResty *resty;

@end


@implementation BIRestyRequest

#pragma mark - Initalization Methods

- (instancetype)initWithResty:(BIResty *)resty andHTTPMethod:(HTTPMethod)method andResource:(NSString *)resource
{
    self = [super init];
    
    if (self)
    {
        self.resty = resty;
        self.HTTPMethod = method;
        self.resource = resource;
    }
    
    return self;
}


#pragma mark - Public Methods

- (id)runRequest
{
    return [self requestWithDictionary:nil];
}

- (id)requestWithDictionary:(NSDictionary *)dictionary
{
    // Build the URL as String
    NSString *URLString = @"";
    
    if (self.resty.APIEndPoint != nil && ![self.resty.APIEndPoint isEqualToString:@""] && self.resty.APIEndPoint.length > 0)
    {
        URLString = [NSString stringWithFormat:@"%@:%i/%@/%@", self.resty.baseURL, self.resty.port, self.resty.APIEndPoint, self.resource];
    }
    else
    {
        URLString = [NSString stringWithFormat:@"%@:%i/%@", self.resty.baseURL, self.resty.port, self.resource];
    }
    
    URLString = [URLString stringByReplacingOccurrencesOfString:@"//" withString:@"/"];
    
    // Create Request
    NSMutableURLRequest *URLRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:URLString] cachePolicy:self.resty.cachePolicy timeoutInterval:self.resty.timoutTimeInterval];
    
    if (self.contentType == nil || self.contentType.length == 0)
        self.contentType = @"application/json";
    
    // Configure Request
    [URLRequest setHTTPMethod:[self convertHTTPMethod:self.HTTPMethod]];
    [URLRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [URLRequest addValue:self.contentType forHTTPHeaderField:@"Content-Type"];
    
    // If we've got some data, add it to our request
    if(dictionary != nil)
    {
        id jsonData = [self convertDictionary:dictionary];
        
        if([jsonData isKindOfClass:[NSData class]])
        {
            [URLRequest setHTTPBody:jsonData];
            [URLRequest addValue:[NSString stringWithFormat:@"%li",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        }
        else
            return jsonData; // Return NSError
    }
    
    // If we've got some custom Auth-Headers, add them!
    if(self.resty.customAuthHeaderField && self.resty.customAuthHeaderValue)
        [URLRequest addValue:self.resty.customAuthHeaderValue forHTTPHeaderField:self.resty.customAuthHeaderField];
    
    
    // Start the request
    NSHTTPURLResponse *URLResponse = nil;
    NSError *error = nil;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:URLRequest returningResponse:&URLResponse error:&error];
    
    if(!error && responseData != nil)
    {
        // Check for good status code
        if (URLResponse.statusCode >= 200 && URLResponse.statusCode < 300)
            return [[BIRestyResponse alloc] initWithRequest:self andResponseData:responseData andResponseCode:URLResponse.statusCode]; // All ok
        else
        {
            NSMutableDictionary *errorContext = [[NSMutableDictionary alloc] initWithDictionary:@{ @"request" : self, @"url" : URLString, @"response": URLResponse }];
            
            if(dictionary != nil)
               [errorContext setObject:dictionary forKey:@"dictionary"];
            
            return [NSError errorWithDomain:@"BIResty" code:URLResponse.statusCode userInfo:errorContext]; // Error Handling, bad status-code
        }
    }
    else
        return error; // Error Handling
    
    
    return NULL;
}


#pragma mark - Utlity Methods

- (id)convertDictionary:(NSDictionary *)dictionary
{
    if(dictionary != nil && [dictionary isKindOfClass:[NSDictionary class]])
    {
        NSError *error = nil;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:kNilOptions error:&error];
        
        if(!error)
            return jsonData;
        else
            return error;
        
    }
    
    return nil;
}

- (NSString *)convertHTTPMethod:(HTTPMethod)method
{
    switch (method)
    {
        case HTTPMethodGET:
            return @"GET";
        case HTTPMethodPOST:
            return @"POST";
        case HTTPMethodPUT:
            return @"PUT";
        case HTTPMethodDELETE:
            return @"DELETE";
        default:
            return @"GET";
    }
    
    return @"GET";
}


@end
