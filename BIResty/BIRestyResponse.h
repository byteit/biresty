//
//  BIRestyResponse.h
//  BIRestyLibrary
//
//  Created by Jan Galler on 04/06/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Globals.h"


@class BIRestyRequest;
@interface BIRestyResponse : NSObject

@property (strong, nonatomic, readonly) BIRestyRequest *request;

@property (strong, nonatomic, readonly) NSData *responseData;
@property (strong, nonatomic, readonly) NSDictionary *responseDict;
@property (strong, nonatomic, readonly) NSArray *responseArray;
@property (nonatomic, readonly) NSInteger responseCode;

- (instancetype)initWithRequest:(BIRestyRequest *)request andResponseData:(NSData *)responseData andResponseCode:(NSInteger)responseCode;


@end
