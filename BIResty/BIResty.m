//
//  BIResty.m
//  BIRestyLibrary
//
//  Created by Jan Galler on 04/06/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import "BIResty.h"

@implementation BIResty

#pragma mark - Initalization Methods

- (id)initWithBaseURL:(NSString *)baseURL andAPIEndPointPath:(NSString *)APIEndPoint
{
    self = [super init];
    if (self)
    {
        self.baseURL = [NSURL URLWithString:baseURL];
        self.APIEndPoint = APIEndPoint;
        self.port = 80;
        self.timoutTimeInterval = 15;
        self.cachePolicy = NSURLCacheStorageNotAllowed;
    }
    
    return self;
}


#pragma mark - Public Methods

#pragma mark Delegate-Style

- (BIRestyResponse *)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andTag:(NSInteger)tag
{
    return [self requestResource:resource withHTTPMethod:method andTag:tag andDictionary:nil];
}

- (BIRestyResponse *)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andTag:(NSInteger)tag andDictionary:(NSDictionary *)data
{
    return [self requestResource:resource withHTTPMethod:method andTag:tag andDictionary:data andContentType:nil];
}

- (BIRestyResponse *)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andTag:(NSInteger)tag andDictionary:(NSDictionary *)data andContentType:(NSString *)contentType
{
    // If we've got a ResponseDelegate that responds to the Request-Finished-Method, call him. Otherwise we're going to return synchronously
    if (self.responseDelegate != nil && [self.responseDelegate respondsToSelector:@selector(restyRequest:didRequestWithResponse:withHTTPMethod:andTag:)])
    {
        // Do the request on background
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
        {
            // Run the request
            id response = [self makeURLRequestWithHTTPMethod:method andResource:resource andDictionary:data andTag:tag andContentType:contentType];

            // Get back into the main thread
            dispatch_async(dispatch_get_main_queue(), ^
            {
                if([response isKindOfClass:[BIRestyResponse class]])
                {
                    BIRestyResponse *restyResponse = (BIRestyResponse *)response;

                    // Finnally, call our delegate and pass our data
                    [self.responseDelegate restyRequest:restyResponse.request didRequestWithResponse:response withHTTPMethod:method andTag:tag];
                }
                else
                {
                    [self.responseDelegate restyRequest:nil didFailWithError:response andResponse:response andTag:tag];
                }

            });
           
        });
        
    }
    else
    {
        // Just do the request synchronously
        return [self makeURLRequestWithHTTPMethod:method andResource:resource andDictionary:data andTag:tag];
    }
    
    
    return NULL;

}

#pragma mark Callback-Style

- (void)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andCallback:(CallbackBlock)callback
{
    [self requestResource:resource withHTTPMethod:method andDictionary:nil andCallback:callback];
}

- (void)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andDictionary:(NSDictionary *)data andCallback:(CallbackBlock)callback
{
    [self requestResource:resource withHTTPMethod:method andDictionary:data andCallback:callback andContentType:nil];
}

- (void)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andDictionary:(NSDictionary *)data andCallback:(CallbackBlock)callback andContentType:(NSString *)contentType
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^
    {
        id response = [self makeURLRequestWithHTTPMethod:method andResource:resource andDictionary:data andTag:-1 andContentType:contentType];

        dispatch_async(dispatch_get_main_queue(), ^
        {
            // Run the callback and pass the response
            callback(response);
        });
       
    });
    
}


#pragma mark - Internal Methods

- (BIRestyResponse *)makeURLRequestWithHTTPMethod:(HTTPMethod)method andResource:(NSString *)resource andDictionary:(NSDictionary *)dictionary
{
    return [self makeURLRequestWithHTTPMethod:method andResource:resource andDictionary:dictionary andTag:-1];
}

- (BIRestyResponse *)makeURLRequestWithHTTPMethod:(HTTPMethod)method andResource:(NSString *)resource andDictionary:(NSDictionary *)dictionary andTag:(NSInteger)tag
{
    return [self makeURLRequestWithHTTPMethod:method andResource:resource andDictionary:dictionary andTag:tag andContentType:nil];

}

- (BIRestyResponse *)makeURLRequestWithHTTPMethod:(HTTPMethod)method andResource:(NSString *)resource andDictionary:(NSDictionary *)dictionary andTag:(NSInteger)tag andContentType:(NSString *)contentType
{
    BIRestyRequest *request = [[BIRestyRequest alloc] initWithResty:self andHTTPMethod:method andResource:resource];

    // Use a custom ContentType if given
    if(contentType != nil && contentType.length > 0)
        request.contentType = contentType;
    
    // If we've got a ResponseDelegate that respons to the Request-Start-Method, call him
    if(self.responseDelegate != nil && [self.responseDelegate respondsToSelector:@selector(restyRequest:willStartWithTag:)])
        [self.responseDelegate restyRequest:request willStartWithTag:tag];
    
    id response;
    
    if(!dictionary)
        response = [request runRequest];
    else
        response = [request requestWithDictionary:dictionary];
    
    // Check for error
    if (![response isKindOfClass:[BIRestyResponse class]])
    {
        // Something failed
        [self.responseDelegate restyRequest:request didFailWithError:response andResponse:response andTag:tag];
        
        return NULL;
    }
    
    if(self.responseDelegate != nil && [self.responseDelegate respondsToSelector:@selector(restyRequest:didRequestWithResponse:withHTTPMethod:andTag:)])
        [self.responseDelegate restyRequest:request didRequestWithResponse:response withHTTPMethod:method andTag:tag];
    
    return response;

}



@end
