//
//  BIRestyRequest.h
//  BIRestyLibrary
//
//  Created by Jan Galler on 04/06/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Globals.h"
#import "BIResty.h"

@class BIRestyResponse, BIResty;
@interface BIRestyRequest : NSObject

@property () HTTPMethod HTTPMethod;
@property (strong, nonatomic) NSString *resource;
@property (strong, nonatomic) NSString *contentType;


// Initialize with all the required stuff
- (instancetype)initWithResty:(BIResty *)resty andHTTPMethod:(HTTPMethod)method andResource:(NSString *)resource;

// Do the request and return a Response
- (id)runRequest;
// Or use this method to also pass data
- (id)requestWithDictionary:(NSDictionary *)dictionary;

@end
