//
//  BIRestyResponse.m
//  BIRestyLibrary
//
//  Created by Jan Galler on 04/06/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import "BIRestyResponse.h"

@implementation BIRestyResponse

#pragma mark - Initalization Methods

- (instancetype)initWithRequest:(BIRestyRequest *)request andResponseData:(NSData *)responseData andResponseCode:(NSInteger)responseCode
{
    self = [super init];
    
    if (self)
    {
        _request = request;
        _responseData = responseData;
        _responseCode = responseCode;
        
        id data = [self convertData:responseData];
        
        if([data isKindOfClass:[NSDictionary class]])
            _responseDict = data;
        else if([data isKindOfClass:[NSArray class]])
            _responseArray = data;
        else
            _responseDict = @{ @"error" : data };
    }
    
    return self;
}


#pragma mark - Utlity Methods

- (id)convertData:(NSData *)data
{
    NSError *error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if (!error)
        return json;
    else
        return error;
    
    return nil;
}


@end
