//
//  BIResty.h
//  BIRestyLibrary
//
//  Created by Jan Galler on 04/06/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Globals.h"
#import "BIRestyRequest.h"
#import "BIRestyResponse.h"


// Callback Type
typedef void (^CallbackBlock)(id response);


// Delegate Interface
@protocol BIRestyResponseDelegate <NSObject>

// Required Delegate Methods
@required
- (void)restyRequest:(BIRestyRequest *)request didFailWithError:(NSObject *)error andResponse:(id)response andTag:(NSInteger)tag;

// Optional Delegate Methods
@optional
- (void)restyRequest:(BIRestyRequest *)request didRequestWithResponse:(BIRestyResponse *)response withHTTPMethod:(HTTPMethod)method andTag:(NSInteger)tag;
- (void)restyRequest:(BIRestyRequest *)request willStartWithTag:(NSInteger )tag;

@end


// Library Interface
@interface BIResty : NSObject

// Required API Properties
@property (strong, readwrite)   NSURL                       *baseURL;
@property (strong, readwrite)   NSString                    *APIEndPoint;
@property ()                    UInt16                      port;
@property ()                    HTTPMethod                  method;

// Connection Meta Properties
@property ()                    NSURLRequestCachePolicy     cachePolicy;
@property ()                    NSTimeInterval              timoutTimeInterval;

// Delegate Properties
@property ()                    id<BIRestyResponseDelegate> responseDelegate;

// Custom Auth Header Field/Value Properties
@property (strong, readwrite)   NSString                    *customAuthHeaderField;
@property (strong, readwrite)   NSString                    *customAuthHeaderValue;


// Public Methods
- (id)initWithBaseURL:(NSString *)baseURL andAPIEndPointPath:(NSString *)APIEndPoint;

- (BIRestyResponse *)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andTag:(NSInteger)tag;
- (BIRestyResponse *)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andTag:(NSInteger)tag andDictionary:(NSDictionary *)data;
- (BIRestyResponse *)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andTag:(NSInteger)tag andDictionary:(NSDictionary *)data andContentType:(NSString *)contentType;

- (void)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andCallback:(CallbackBlock)callback;
- (void)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andDictionary:(NSDictionary *)data andCallback:(CallbackBlock)callback;
- (void)requestResource:(NSString *)resource withHTTPMethod:(HTTPMethod)method andDictionary:(NSDictionary *)data andCallback:(CallbackBlock)callback andContentType:(NSString *)contentType;


@end
