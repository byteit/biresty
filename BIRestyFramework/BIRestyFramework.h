//
//  BIRestyFramework.h
//  BIRestyFramework
//
//  Created by Jan Galler on 04/06/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for BIRestyFramework.
FOUNDATION_EXPORT double BIRestyFrameworkVersionNumber;

//! Project version string for BIRestyFramework.
FOUNDATION_EXPORT const unsigned char BIRestyFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BIRestyFramework/PublicHeader.h>

#import "BIResty.h"